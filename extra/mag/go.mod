module git.archive.org/webgroup/refcat/extra/mag

go 1.17

require (
	github.com/jmoiron/sqlx v1.3.4
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/miku/parallel v0.0.0-20210205192328-1a799ab70294
)
