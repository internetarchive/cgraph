# Sample CDX Links

Given a sample from outbound web links from publications, determine number of
URLs we may have. We currently find about 44368911 URLs in the refs.

Limit to 10000 links.

10k random:

```
   7010 OK
   2940 MISS
```

10k w/o doi.org:

```
   6442 OK
   3480 MISS
```
