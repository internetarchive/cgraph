package xio

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
)

// SyncWriter makes any writer thread-safe using a lock.
type SyncWriter struct {
	sync.Mutex
	w io.Writer
}

// NewSyncWriter returns an io.Writer that can be safely written to by multiple
// goroutines.
func NewSyncWriter(w io.Writer) *SyncWriter {
	return &SyncWriter{w: w}
}

// Write wraps the underlying writer and gives exclusive access.
func (w *SyncWriter) Write(p []byte) (n int, err error) {
	w.Lock()
	n, err = w.w.Write(p)
	w.Unlock()
	return
}

// OpenTwo opens two files. The caller needs to check for a single error only.
func OpenTwo(f0, f1 string) (g0, g1 *os.File, err error) {
	if g0, err = os.Open(f0); err != nil {
		return nil, nil, err
	}
	if g1, err = os.Open(f1); err != nil {
		return nil, nil, err
	}
	return g0, g1, nil
}

// MapFromTabular reads from a reader and turns values from keyC, valueC
// columns, both 1-indexed, into a map.
func MapFromTabular(r io.Reader, sep string, keyC, valueC int) (map[string]string, error) {
	var (
		br         = bufio.NewReader(r)
		m          = make(map[string]string)
		line, k, v string
		fields     []string
		err        error
	)
	for {
		line, err = br.ReadString('\n')
		if err == io.EOF {
			return m, nil
		}
		if err != nil {
			return nil, err
		}
		fields = strings.Split(line, sep)
		if len(fields) > keyC-1 && len(fields) > valueC-1 {
			k = strings.TrimSpace(fields[keyC-1])
			v = strings.TrimSpace(fields[valueC-1])
			m[k] = v
		} else {
			return nil, fmt.Errorf("invalid line: %v (%v fields, %v, %v)",
				line, len(fields), keyC, valueC)
		}
	}
	return m, nil
}
