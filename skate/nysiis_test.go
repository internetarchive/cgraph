package skate

import "testing"

func TestNYSIIS(t *testing.T) {
	var cases = []struct {
		s      string
		result string
	}{
		{"", ""},
		{"hello", "HAL"},
		{"world", "WARLD"},
		{"john", "JAN"},
		{"jon", "JAN"},
		{"jon???", "JAN"},
		{"Jonn", "JAN"},
	}
	for _, c := range cases {
		result := NYSIIS(c.s)
		if result != c.result {
			t.Fatalf("got %v, want %v", result, c.result)
		}
	}
}
