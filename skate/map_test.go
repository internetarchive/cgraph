package skate

import (
	"reflect"
	"testing"
)

func TestMapperTitle(t *testing.T) {
	var cases = []struct {
		b      string
		result []string
		err    error
	}{
		{
			b:      "{}",
			result: []string{"", "{}"},
			err:    nil,
		},
		{
			b:      `{"title": "Hello"}`,
			result: []string{"Hello", `{"title": "Hello"}`},
			err:    nil,
		},
		{
			b: `{"title": "This is not_**normalized"}`,
			result: []string{
				"This is not_**normalized",
				`{"title": "This is not_**normalized"}`,
			},
			err: nil,
		},
	}
	for _, c := range cases {
		result, err := MapperTitle([]byte(c.b))
		if !reflect.DeepEqual(prettySlice(result), c.result) {
			t.Fatalf("got %v, want %v", result, c.result)
		}
		if err != c.err {
			t.Fatalf("got %v, want %v", err, c.err)
		}
	}
}

func TestMapperTitleNormalized(t *testing.T) {
	var cases = []struct {
		b      string
		result []string
		err    error
	}{
		{
			b:      "{}",
			result: []string{"", "{}"},
			err:    nil,
		},
		{
			b:      `{"title": "Hello"}`,
			result: []string{"hello", `{"title": "Hello"}`},
			err:    nil,
		},
		{
			b: `{"title": "This is ..._**normalized"}`,
			result: []string{
				"thisis_normalized",
				`{"title": "This is ..._**normalized"}`,
			},
			err: nil,
		},
	}
	for _, c := range cases {
		result, err := MapperTitleNormalized([]byte(c.b))
		if !reflect.DeepEqual(prettySlice(result), c.result) {
			t.Fatalf("got %v, want %v", prettySlice(result), c.result)
		}
		if err != c.err {
			t.Fatalf("got %v, want %v", err, c.err)
		}
	}
}

func TestMapperTitleNysiis(t *testing.T) {
	var cases = []struct {
		b      string
		result []string
		err    error
	}{
		{
			b:      "{}",
			result: []string{"", "{}"},
			err:    nil,
		},
		{
			b:      `{"title": "Hello"}`,
			result: []string{"HAL", `{"title": "Hello"}`},
			err:    nil,
		},
		{
			b: `{"title": "This is ..._**normalized"}`,
			result: []string{
				"TASASN",
				`{"title": "This is ..._**normalized"}`,
			},
			err: nil,
		},
	}
	for _, c := range cases {
		result, err := MapperTitleNysiis([]byte(c.b))
		if !reflect.DeepEqual(prettySlice(result), c.result) {
			t.Fatalf("got %v, want %v", prettySlice(result), c.result)
		}
		if err != c.err {
			t.Fatalf("got %v, want %v", err, c.err)
		}
	}
}

func TestMapperTitleSandcrawler(t *testing.T) {
	var cases = []struct {
		b      string
		result []string
		err    error
	}{
		{
			b:      "{}",
			result: []string{"", "{}"},
			err:    nil,
		},
		{
			b:      `{"title": "Hello"}`,
			result: []string{"hello", `{"title": "Hello"}`},
			err:    nil,
		},
		{
			b: `{"title": "This is ..._**normalized"}`,
			result: []string{
				"thisisnormalized",
				`{"title": "This is ..._**normalized"}`,
			},
			err: nil,
		},
	}
	for _, c := range cases {
		result, err := MapperTitleSandcrawler([]byte(c.b))
		if !reflect.DeepEqual(prettySlice(result), c.result) {
			t.Fatalf("got %v, want %v", prettySlice(result), c.result)
		}
		if err != c.err {
			t.Fatalf("got %v, want %v", err, c.err)
		}
	}
}

func TestAsTSV(t *testing.T) {
	var cases = []struct {
		f    Mapper
		err  error
		want string
	}{
		{
			f: Mapper(func(_ []byte) ([][]byte, error) {
				return [][]byte{
					[]byte("a"),
					[]byte("b"),
					[]byte("c"),
				}, nil
			}),
			err:  nil,
			want: "a\tb\tc\n",
		},
		{
			f: Mapper(func(_ []byte) ([][]byte, error) {
				return [][]byte{
					[]byte("a"),
					[]byte("b"),
					[]byte("c\n"),
				}, nil
			}),
			err:  nil,
			want: "a\tb\tc\n",
		},
	}
	for _, c := range cases {
		got, err := c.f.AsTSV([]byte{})
		if err != c.err {
			t.Fatalf("got %v, want nil", got)
		}
		if string(got) != c.want {
			t.Fatalf("got %v, want %v", string(got), c.want)
		}
	}
}

func prettySlice(p [][]byte) (result []string) {
	result = make([]string, len(p))
	for i, v := range p {
		result[i] = string(v)
	}
	return result
}
