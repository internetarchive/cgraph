# 2023 refcat update

* [ ] new refs export, estimated ~3.5B
* [ ] new fatcat export
* [ ] new openlibrary export
* [ ] new wikipedia citation extraction from 2023: https://zenodo.org/records/8107239


