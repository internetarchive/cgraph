# Data quality issues

## Mismatched PDFs

* https://fatcat.wiki/release/iwvep44l5jdqlpwrzjmfdtxdrio

Two SHA1, titles match roughly, but these are different docs:

* [https://file.scirp.org/pdf/JSS_2017121115100474.pdf](https://file.scirp.org/pdf/JSS_2017121115100474.pdf)
* [http://eprints.lincoln.ac.uk/id/eprint/38840/1/polcice%20journal.pdf](http://eprints.lincoln.ac.uk/id/eprint/38840/1/polcice%20journal.pdf)

